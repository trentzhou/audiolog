from django.conf.urls import url

from alog.views import PostListView, PostItemView, PostCreateView

urlpatterns = [
    url(r'posts$', PostListView.as_view(), name="post_list"),
    url(r'new$', PostCreateView.as_view(), name="post_create"),
    url(r'posts/(?P<post_id>\d+)$', PostItemView.as_view(), name="post_item"),
]