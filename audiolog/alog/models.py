from django.db import models

# Create your models here.
from utils import get_now


class AudioClip(models.Model):
    """
    An audio clip
    """
    file = models.FileField(verbose_name="文件", blank=False, max_length=200)

    def __repr__(self):
        return "Audio file {0}".format(self.file.name)

    def __str__(self):
        return self.__repr__()


class Post(models.Model):
    """
    A log post.
    """
    title = models.CharField(blank=False, max_length=200, verbose_name="标题")
    summary = models.CharField(blank=True, default="", max_length=200, verbose_name="概要")
    content = models.TextField(blank=True, default="", verbose_name="内容")
    audio = models.ForeignKey(AudioClip, on_delete=models.CASCADE, verbose_name="音频", null=True)
    create_time = models.DateTimeField(default=get_now, verbose_name="创建时间")

    def __repr__(self):
        return "Post {0} - {1}".format(self.create_time, self.title)

    def __str__(self):
        return self.__repr__()


class Comment(models.Model):
    """
    A comment
    """
    post = models.ForeignKey(Post, on_delete=models.CASCADE, verbose_name="文章")
    email = models.EmailField(blank=True, verbose_name="Email")
    nick_name = models.CharField(blank=True, default="", max_length=200, verbose_name="昵称")
    content = models.TextField(blank=True, default="", verbose_name="内容")
    create_time = models.DateTimeField(default=get_now, verbose_name="创建时间")

    def __repr__(self):
        return "Comment by {0} - {1}".format(self.email, self.content)

    def __str__(self):
        return self.__repr__()
