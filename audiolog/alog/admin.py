from django.contrib import admin

# Register your models here.
from alog.models import Post, AudioClip, Comment

admin.site.register(Post)
admin.site.register(AudioClip)
admin.site.register(Comment)
