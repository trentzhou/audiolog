import datetime
import os

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
import markdown
import base64

# Create your views here.
from django.urls import reverse
from django.views import View

from alog.models import Post, AudioClip


class PostCreateView(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'post-create.html')

    def post(self, request):
        title = request.POST["title"]
        brief = request.POST["brief"]
        text = request.POST["text"]

        post = Post()
        post.title = title
        post.summary = brief
        post.content = text

        now = datetime.datetime.now()
        filename = now.strftime("%Y-%m-%d-%H-%M-%S.%f") + ".mp3"
        # TODO: write the file
        folder = os.path.join(settings.MEDIA_ROOT, 'uploads')
        if not os.path.exists(folder):
            os.makedirs(folder, exist_ok=True)
        save_path = os.path.join(folder, filename)

        audio_data = request.POST.get("audioData", None)
        file_saved = False

        if "audio_file" in request.FILES:
            with open(save_path, "wb") as f:
                for chunk in request.FILES["audio_file"].chunks():
                    f.write(chunk)
                file_saved = True
        elif audio_data:
            comma = audio_data.index(",")
            encoded_data = audio_data[comma+1:]
            data = base64.standard_b64decode(encoded_data)

            with open(save_path, "wb") as f:
                f.write(data)
                file_saved = True
        if file_saved:
            audio_clip = AudioClip()
            audio_clip.file.name = os.path.join("uploads/", filename)
            audio_clip.save()
            post.audio = audio_clip

        post.save()
        return redirect(reverse('post_list'))


class PostListView(View):
    def get(self, request):
        posts = Post.objects.order_by("-create_time").all()
        return render(request, 'index.html', {
            "posts": posts,
            "title": "声音日记"
        })


class PostItemView(View):
    def get(self, request, post_id):
        post = Post.objects.get(id=post_id)
        content = markdown.markdown(post.content, safe_mode=True, extensions=['extra', 'codehilite', 'toc'])
        return render(request, 'post.html', {
            "post": post,
            "title": post.title,
            "content": content
        })

