#!/usr/bin/env bash 
set -x
IMAGE=trentzhou/audiolog
if [ -n "$http_proxy" ]; then
    EXTRA_OPTIONS="--build-arg HTTP_PROXY=$http_proxy --build-arg HTTPS_PROXY=$http_proxy --build-arg http_proxy=$http_proxy --build-arg https_proxy=$https_proxy"
fi

docker build -t $IMAGE $EXTRA_OPTIONS .

if [ -n "$DOCKER_PUBLISH_IMAGE" ]; then
    docker push $IMAGE
fi
